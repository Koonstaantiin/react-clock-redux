import React from 'react'
import { connect } from 'react-redux'
import Moment from 'moment'

class HotelClock extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      date: Moment().add(this.props.timeOffset, 'hours').toDate()
    }
  }
  
  componentDidUpdate(prevProps) {
    if (prevProps.newDate !== this.props.newDate) {
      this.updateTime()
    }
  }

  updateTime = () => {
    const { timeOffset, newDate } = this.props
    
    const date = Moment(newDate).add(timeOffset, 'hours').toDate()

    this.setState(
      ({ date }), 
      () => this.rotateArrows()
    )
  }

  rotateArrows() {
    const { date } = this.state

    this.rotateArrow(this.refSeconds, 6 * date.getSeconds())
    this.rotateArrow(this.refMinutes, 6 * date.getMinutes())
    this.rotateArrow(this.refHours, 30 * (date.getHours() % 12) + date.getMinutes() / 2)
  }

  rotateArrow(element, degrees) {
    element.setAttribute('transform', 'rotate(' + degrees + ' 50 50)')
  }

  render() {
    const { timeOffset } = this.props
    const { date } = this.state
    const dateFormatted = Moment(date).format('DD.MM.YYYY HH:mm:ss')

    return (
      <div className={'clock-wrapper'}>
        <div>Смещение: {timeOffset}</div>
        <div>Время: { dateFormatted }</div>
        <svg className={'clock'} viewBox="0 0 100 100">
          <circle className={'clock-face'} cx="50" cy="50" r="45"/>
          <g className={'arrows'}>
            <rect ref={(element) => this.refHours = element} className={ 'arrow-hours' } x="48.5" y="12.5" width="3" height="40" rx="2.5" ry="2.55" />
            <rect ref={(element) => this.refMinutes = element} className={ 'arrow-minutes' } x="48" y="12.5" width="3" height="40" rx="2" ry="2"/>
            <line ref={(element) => this.refSeconds = element} className={ 'arrow-seconds' } x1="50" y1="50" x2="50" y2="16" />
          </g>
        </svg>
      </div>
    )
  }

}

HotelClock.defaultProps = {
  timeOffset: '0'
}

function mapStateToProps(state, ownProps) {
  return {
    newDate: state.newDate
  }
}

export default connect(
  mapStateToProps
)(HotelClock)