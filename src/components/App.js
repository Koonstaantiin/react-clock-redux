import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setNewDate } from '../actions/clockActions'
import HotelClock from './HotelClock.js'
import '../styles/App.css'

export class App extends React.Component {
  componentDidMount () {
    const { setNewDate } = this.props
    setNewDate()
    setInterval(setNewDate, 1000)
  }

  render () {
    return (
      <div className="App" id="clockApp">
        <React.Fragment>
            <HotelClock />
            <HotelClock timeOffset="5"/>
            <HotelClock timeOffset="-3"/>
        </React.Fragment>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setNewDate: bindActionCreators(setNewDate, dispatch)
  }
}

export default connect(
  null,
  mapDispatchToProps
)(App)
