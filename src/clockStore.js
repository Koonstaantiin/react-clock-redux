import { createStore } from 'redux'
import rootReducer from './reducers'

export default function clockStoreConfigure(initialState) {
  return createStore(
    rootReducer,
    initialState
  )
}