import Moment from 'moment'

export default {
  newDate: Moment().toDate()
}