import Moment from 'moment/moment'
import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function newDateReducer(state = initialState.newDate, action) {
  switch(action.type) {
    case types.SET_NEW_DATE:
      return Moment().toDate();
    default:
     return state;
  }
}