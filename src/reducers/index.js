import { combineReducers } from 'redux'
import newDate from './newDateReducer'

const rootReducer = combineReducers({
  newDate
})

export default rootReducer