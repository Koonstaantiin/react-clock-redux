import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import clockStore from './clockStore'
import App from './components/App'

const store = clockStore()

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)