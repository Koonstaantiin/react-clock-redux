import { SET_NEW_DATE } from './actionTypes'

export function setNewDate() {
  return { type: SET_NEW_DATE }
}